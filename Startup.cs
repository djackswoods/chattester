﻿//using Microsoft.Owin;
//using Owin;

//[assembly: OwinStartupAttribute(typeof(ChatTester.Startup))]
//namespace ChatTester
//{
//    public partial class Startup
//    {
//        public void Configuration(IAppBuilder app)
//        {
//            ConfigureAuth(app);
//        }
//    }
//}

using Microsoft.Owin;
using Owin;
//[assembly: OwinStartupAttribute(typeof(ChatTester.Startup))]
[assembly: OwinStartup(typeof(SignalRChat.Startup))]

namespace SignalRChat
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Any connection or hub wire up and configuration should go here
            app.MapSignalR();
        }

        
    }
}
